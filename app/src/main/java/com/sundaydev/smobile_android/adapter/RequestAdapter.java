package com.sundaydev.smobile_android.adapter;

import android.content.Context;
import android.text.TextUtils;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.sundaydev.smobile_android.request.IReqBase;

import org.json.JSONException;
import org.json.JSONObject;

/**
 * Created by simsooltong on 2017. 8. 11..
 */

public class RequestAdapter {

    RequestQueue queue;
    Response.Listener<JSONObject> resultListener;
    Response.ErrorListener errorListener;

    public RequestAdapter(Context context) {
        queue = Volley.newRequestQueue(context);
    }

    public RequestAdapter setResponseListener(Response.Listener<JSONObject> resultLinster) {
        this.resultListener = resultLinster;
        return this;
    }

    public RequestAdapter setErrorListener(Response.ErrorListener errorListener) {
        this.errorListener = errorListener;
        return this;
    }

    public RequestAdapter request(IReqBase iReqBase) throws NullPointerException {
        String url = iReqBase.getUrl();
        String method = iReqBase.getMethod();
        String jsonStr = iReqBase.getParams();

        if(TextUtils.isEmpty(url) || TextUtils.isEmpty(method) || TextUtils.isEmpty(jsonStr))
            new NullPointerException();

        JSONObject jsonObject = null;

        try {
            jsonObject = new JSONObject(jsonStr);
        } catch (JSONException e) {
            e.printStackTrace();
        }

        int methodType = method.equalsIgnoreCase("post") ?  Request.Method.POST : Request.Method.GET;
        final JsonObjectRequest request = new JsonObjectRequest(methodType, url, jsonObject, resultListener, errorListener);
        queue.add(request);
        return this;
    }
}
