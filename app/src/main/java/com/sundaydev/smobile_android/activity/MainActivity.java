package com.sundaydev.smobile_android.activity;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.sundaydev.smobile_android.R;
import com.sundaydev.smobile_android.adapter.RequestAdapter;
import com.sundaydev.smobile_android.request.IReqBase;
import com.sundaydev.smobile_android.request.data.ParamA;
import com.sundaydev.smobile_android.request.data.ParamB;
import com.sundaydev.smobile_android.request.data.ReqA;
import com.sundaydev.smobile_android.request.data.ReqB;
import com.sundaydev.smobile_android.request.data.ReqBase;

import org.json.JSONObject;

public class MainActivity extends AppCompatActivity implements View.OnClickListener {
    private EditText editText;
    private Button btnServerA;
    private Button btnServerB;
    private Button btnServerBoth;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        editText = (EditText) findViewById(R.id.etText);

        btnServerA = (Button) findViewById(R.id.btnServerA);
        btnServerB = (Button) findViewById(R.id.btnServerB);
        btnServerBoth = (Button) findViewById(R.id.btnServerBoth);
        btnServerA.setOnClickListener(this);
        btnServerB.setOnClickListener(this);
        btnServerBoth.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        if(v == btnServerA) {
            sendA();
        } else if(v == btnServerB) {
            sendB();
        } else if(v == btnServerBoth) {
            sendA();
            sendB();
        } else {

        }
    }

    private void sendB() {
        CharSequence sendMsg = getSendMsg();
        if (sendMsg == null) return;

        final ReqBase<ParamB> reqB = new ReqB();
        if(reqB != null) {
            reqB.set(new ParamB(sendMsg.toString()));
            sendData(reqB);
        }
    }

    private void sendA() {
        CharSequence sendMsg = getSendMsg();
        if (sendMsg == null) return;

        final ReqBase<ParamA> reqA = new ReqA();
        if(reqA != null) {
            reqA.set(new ParamA(sendMsg.toString()));
            sendData(reqA);
        }
    }

    @Nullable
    private CharSequence getSendMsg() {
        if(editText == null)
            return null;

        CharSequence sendMsg = editText.getText();
        if(TextUtils.isEmpty(sendMsg))
            return null;
        return sendMsg;
    }

    private void sendData(final IReqBase iReqBase) {
        new RequestAdapter(MainActivity.this).setResponseListener(new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                Log.i("sss", String.format("url : %s, params : %s, msg : %s", iReqBase.getUrl(), iReqBase.getParams(), response.toString()));
            }
        }).setErrorListener(new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.i("sss", String.format("url : %s, params : %s, msg : %s", iReqBase.getUrl(), iReqBase.getParams(), error.getMessage()));
            }
        }).request(iReqBase);
    }
}
