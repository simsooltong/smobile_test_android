package com.sundaydev.smobile_android.request.data;

import com.google.gson.annotations.Expose;

/**
 * Created by simsooltong on 2017. 8. 10..
 */

public class ParamB {
    @Expose
    public String content;

    public ParamB(String content) {
        this.content = content;
    }
}
