package com.sundaydev.smobile_android.request.data;

import android.text.TextUtils;

import com.google.gson.Gson;
import com.google.gson.JsonSyntaxException;
import com.google.gson.annotations.Expose;
import com.sundaydev.smobile_android.request.IReqBase;

/**
 * Created by simsooltong on 2017. 8. 10..
 */

public abstract class ReqBase<T> implements IReqBase {

    T params;
    public ReqBase set(T params) {this.params = params; return this; }
    public T get() {return params; }

    @Expose
    String url;
    @Expose
    String method;
    @Expose
    String valueStr;

    @Override
    public String getMethod() {
        return "POST";
    }

    @Override
    public String getUrl() {
        return url;
    }

    @Override
    public String getParams() {
        Gson gson = new Gson();
        String jsonStr = "";

        try {
            jsonStr = gson.toJson(params);
        }catch (JsonSyntaxException e) {
            e.printStackTrace();
            return null;
        }
        return TextUtils.isEmpty(jsonStr) ? null : jsonStr;
    }

    public ReqBase setUrl(String url){
        this.url = url;
        return this;
    }

    public ReqBase setMethod(String method) {
        this.method = method;
        return this;
    }

    public ReqBase setParam(String valueStr) {
        this.valueStr = valueStr;
        return this;
    }
}
