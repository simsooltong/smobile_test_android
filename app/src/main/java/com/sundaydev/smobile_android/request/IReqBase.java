package com.sundaydev.smobile_android.request;

/**
 * Created by simsooltong on 2017. 8. 10..
 */

public interface IReqBase<T> {
    String getUrl();
    String getMethod();
    String getParams();
}
