package com.sundaydev.smobile_android.request.data;

import com.google.gson.annotations.Expose;

/**
 * Created by simsooltong on 2017. 8. 10..
 */

public class ParamA {
    @Expose
    public String body;

    public ParamA(String body) {
        this.body = body;
    }
}
